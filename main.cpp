#include <iostream>
#include <math.h>

using namespace std;

class Low
{
protected:
	double min, max, problem, median;
	double degree;

public:
	Low(double MIN, double MAX, double PROBLEM)
	{ // Constructor
		min = MIN;
		max = MAX;
		problem = PROBLEM;
		median = min + ((max - min) / 2);
	}
	double getDegree()
	{ // Getter
		if (problem <= median)
		{
			degree = 1;
		}
		if ((problem > median) && (problem < max))
		{
			degree = (double)(max - problem) / (double)(max - median);
		}
		if (problem >= max)
		{
			degree = 0;
		}
		return degree;
	}
};

class Medium
{
protected:
	double min, max, problem, median;
	double degree;

public:
	Medium(double MIN, double MAX, double PROBLEM)
	{ // Constructor
		min = MIN;
		max = MAX;
		problem = PROBLEM;
		median = min + ((max - min) / 2);
	}
	double getDegree()
	{ // Getter
		if ((problem < min) || (problem > max))
		{
			degree = 0;
		}
		if ((problem >= min) && (problem < median))
		{
			degree = (double)(problem - min) / (double)(median - min);
		}
		if (problem == median)
		{
			degree = 1;
		}
		if ((problem > median) && (problem <= max))
		{
			degree = (double)(max - problem) / (double)(max - median);
		}
		return degree;
	}
};

class High
{
protected:
	double min, max, problem, median;
	double degree;

public:
	High(double MIN, double MAX, double PROBLEM)
	{ // Constructor
		min = MIN;
		max = MAX;
		problem = PROBLEM;
		median = min + ((max - min) / 2);
	}
	double getDegree()
	{ // Getter
		if (problem >= median)
		{
			degree = 1;
		}
		if ((problem < median) && (problem > min))
		{
			degree = (double)(problem - min) / (double)(median - min);
		}
		if (problem <= min)
		{
			degree = 0;
		}
		return degree;
	}
};

class FirstRule // kelas aturan pertama
{				// "JIKA NodeSize LOW dan Mobility LOW maka helloIntervalTime = HIGH"
public:			// hak akses kelas
	double alpha(double minLowNodeSize, double maxLowNodeSize, double NodeSize,
				 double minLowMobility, double maxLowMobility, double Mobility)
	{
		Low NodeSizeLow(minLowNodeSize, maxLowNodeSize, NodeSize);
		Low MobilityLow(minLowMobility, maxLowMobility, Mobility);
		cout << "FirstRule  : "
			 << NodeSizeLow.getDegree()
			 << ", "
			 << MobilityLow.getDegree()
			 << endl;
		return min(NodeSizeLow.getDegree(), MobilityLow.getDegree());
	};
};

class SecondRule // kelas aturan kedua
{				 // "JIKA NodeSize LOW dan Mobility MEDIUM maka helloIntervalTime = HIGH"
public:			 // hak akses kelas
	double alpha(double minLowNodeSize, double maxLowNodeSize, double NodeSize,
				 double minMediumMobility, double maxMediumMobility, double Mobility)
	{
		Low NodeSizeLow(minLowNodeSize, maxLowNodeSize, NodeSize);
		Medium MobilityMedium(minMediumMobility, maxMediumMobility, Mobility);
		cout << "SecondRule : "
			 << NodeSizeLow.getDegree()
			 << ", "
			 << MobilityMedium.getDegree()
			 << endl;
		return min(NodeSizeLow.getDegree(), MobilityMedium.getDegree());
	};
};

class ThirdRule // kelas aturan ketiga
{				// "JIKA NodeSize LOW dan Mobility HIGH maka helloIntervalTime = MEDIUM"
public:			// hak akses kelas
	double alpha(double minLowNodeSize, double maxLowNodeSize, double NodeSize,
				 double minHighMobility, double maxHighMobility, double Mobility)
	{
		Low NodeSizeLow(minLowNodeSize, maxLowNodeSize, NodeSize);
		High MobilityHigh(minHighMobility, maxHighMobility, Mobility);
		cout << "ThirdRule  : "
			 << NodeSizeLow.getDegree()
			 << ", "
			 << MobilityHigh.getDegree()
			 << endl;
		return min(NodeSizeLow.getDegree(), MobilityHigh.getDegree());
	};
};

class FourthRule // kelas aturan keempat
{				 // "JIKA NodeSize MEDIUM dan Mobility LOW maka helloIntervalTime = MEDIUM"
public:			 // hak akses kelas
	double alpha(double minMediumNodeSize, double maxMediumNodeSize, double NodeSize,
				 double minLowMobility, double maxLowMobility, double Mobility)
	{
		Medium NodeSizeMedium(minMediumNodeSize, maxMediumNodeSize, NodeSize);
		Low MobilityLow(minLowMobility, maxLowMobility, Mobility);
		cout << "FourthRule : "
			 << NodeSizeMedium.getDegree()
			 << ", "
			 << MobilityLow.getDegree()
			 << endl;
		return min(NodeSizeMedium.getDegree(), MobilityLow.getDegree());
	};
};

class FifthRule // kelas aturan kelima
{				// "JIKA NodeSize MEDIUM dan Mobility MEDIUM maka helloIntervalTime = MEDIUM"
public:			// hak akses kelas
	double alpha(double minMediumNodeSize, double maxMediumNodeSize, double NodeSize,
				 double minMediumMobility, double maxMediumMobility, double Mobility)
	{
		Medium NodeSizeMedium(minMediumNodeSize, maxMediumNodeSize, NodeSize);
		Medium MobilityMedium(minMediumMobility, maxMediumMobility, Mobility);
		cout << "FifthRule  : "
			 << NodeSizeMedium.getDegree()
			 << ", "
			 << MobilityMedium.getDegree()
			 << endl;
		return min(NodeSizeMedium.getDegree(), MobilityMedium.getDegree());
	};
};

class SixthRule // kelas aturan keenam
{				// "JIKA NodeSize MEDIUM dan Mobility HIGH maka helloIntervalTime = LOW"
public:			// hak akses kelas
	double alpha(double minMediumNodeSize, double maxMediumNodeSize, double NodeSize,
				 double minHighMobility, double maxHighMobility, double Mobility)
	{
		Medium NodeSizeMedium(minMediumNodeSize, maxMediumNodeSize, NodeSize);
		High MobilityHigh(minHighMobility, maxHighMobility, Mobility);
		cout << "SixthRule  : "
			 << NodeSizeMedium.getDegree()
			 << ", "
			 << MobilityHigh.getDegree()
			 << endl;
		return min(NodeSizeMedium.getDegree(), MobilityHigh.getDegree());
	};
};

class SeventhRule // kelas aturan ketujuh
{				  // "JIKA NodeSize HIGH dan Mobility LOW maka helloIntervalTime = LOW"
public:			  // hak akses kelas
	double alpha(double minHighNodeSize, double maxHighNodeSize, double NodeSize,
				 double minLowMobility, double maxLowMobility, double Mobility)
	{
		High NodeSizeHigh(minHighNodeSize, maxHighNodeSize, NodeSize);
		Low MobilityLow(minLowMobility, maxLowMobility, Mobility);
		cout << "SeventhRule: "
			 << NodeSizeHigh.getDegree()
			 << ", "
			 << MobilityLow.getDegree()
			 << endl;
		return min(NodeSizeHigh.getDegree(), MobilityLow.getDegree());
	};
};

class EighthRule // kelas aturan kedelapan
{				 // "JIKA NodeSize HIGH dan Mobility MEDIUM maka helloIntervalTime = LOW"
public:			 // hak akses kelas
	double alpha(double minHighNodeSize, double maxHighNodeSize, double NodeSize,
				 double minMediumMobility, double maxMediumMobility, double Mobility)
	{
		High NodeSizeHigh(minHighNodeSize, maxHighNodeSize, NodeSize);
		Medium MobilityMedium(minMediumMobility, maxMediumMobility, Mobility);
		cout << "EighthRule : "
			 << NodeSizeHigh.getDegree()
			 << ", "
			 << MobilityMedium.getDegree()
			 << endl;
		return min(NodeSizeHigh.getDegree(), MobilityMedium.getDegree());
	};
};

class NinthRule // kelas aturan kesembilan
{				// "JIKA NodeSize HIGH dan Mobility HIGH maka helloIntervalTime = LOW"
public:			// hak akses kelas
	double alpha(double minHighNodeSize, double maxHighNodeSize, double NodeSize,
				 double minHighMobility, double maxHighMobility, double Mobility)
	{
		High NodeSizeHigh(minHighNodeSize, maxHighNodeSize, NodeSize);
		High MobilityHigh(minHighMobility, maxHighMobility, Mobility);
		cout << "NinthRule  : "
			 << NodeSizeHigh.getDegree()
			 << ", "
			 << MobilityHigh.getDegree()
			 << endl;
		return min(NodeSizeHigh.getDegree(), MobilityHigh.getDegree());
	};
};

double deffuzifikasi(double NodeSize, double Mobility)
{
	double minLowNodeSize = 50;		// batasan minimum NodeSize
	double maxLowNodeSize = 100;	// batasan maksimum NodeSize
	double minMediumNodeSize = 75;	// batasan maksimum NodeSize
	double maxMediumNodeSize = 125; // batasan maksimum NodeSize
	double minHighNodeSize = 100;	// batasan maksimum NodeSize
	double maxHighNodeSize = 150;	// batasan maksimum NodeSize

	double minLowMobility = 0;		 // batasan minimum Mobility
	double maxLowMobility = 2.5;	 // batasan maksimum Mobility
	double minMediumMobility = 1.25; // batasan maksimum Mobility
	double maxMediumMobility = 3.75; // batasan maksimum Mobility
	double minHighMobility = 2.5;	 // batasan maksimum Mobility
	double maxHighMobility = 5;		 // batasan maksimum Mobility

	double lowHelloIntervalTime = 1;	// low hello interval time
	double mediumHelloIntervalTime = 3; // medium hello interval time
	double highHelloIntervalTime = 5;	// high hello interval time

	FirstRule firstRule;
	SecondRule secondRule;
	ThirdRule thirdRule;
	FourthRule fourthRule;
	FifthRule fifthRule;
	SixthRule sixthRule;
	SeventhRule seventhRule;
	EighthRule eighthRule;
	NinthRule ninthRule;

	double alphaFirstRule = firstRule.alpha(minLowNodeSize, maxLowNodeSize, NodeSize,
											minLowMobility, maxLowMobility, Mobility);
	double alphaSecondRule = secondRule.alpha(minLowNodeSize, maxLowNodeSize, NodeSize,
											  minMediumMobility, maxMediumMobility, Mobility);
	double alphaThirdRule = thirdRule.alpha(minLowNodeSize, maxLowNodeSize, NodeSize,
											minHighMobility, maxHighMobility, Mobility);
	double alphaFourthRule = fourthRule.alpha(minMediumNodeSize, maxMediumNodeSize, NodeSize,
											  minLowMobility, maxLowMobility, Mobility);
	double alphaFifthRule = fifthRule.alpha(minMediumNodeSize, maxMediumNodeSize, NodeSize,
											minMediumMobility, maxMediumMobility, Mobility);
	double alphaSixthRule = sixthRule.alpha(minMediumNodeSize, maxMediumNodeSize, NodeSize,
											minHighMobility, maxHighMobility, Mobility);
	double alphaSeventhRule = seventhRule.alpha(minHighNodeSize, maxHighNodeSize, NodeSize,
												minLowMobility, maxLowMobility, Mobility);
	double alphaEighthRule = eighthRule.alpha(minHighNodeSize, maxHighNodeSize, NodeSize,
											  minMediumMobility, maxMediumMobility, Mobility);
	double alphaNinthRule = ninthRule.alpha(minHighNodeSize, maxHighNodeSize, NodeSize,
											minHighMobility, maxHighMobility, Mobility);

	double resultFirstRule = alphaFirstRule * highHelloIntervalTime;
	double resultSecondRule = alphaSecondRule * highHelloIntervalTime;
	double resultThirdRule = alphaThirdRule * mediumHelloIntervalTime;
	double resultFourthRule = alphaFourthRule * highHelloIntervalTime;
	double resultFifthRule = alphaFifthRule * mediumHelloIntervalTime;
	double resultSixthRule = alphaSixthRule * lowHelloIntervalTime;
	double resultSeventhRule = alphaSeventhRule * mediumHelloIntervalTime;
	double resultEighthRule = alphaEighthRule * lowHelloIntervalTime;
	double resultNinthRule = alphaNinthRule * lowHelloIntervalTime;

	cout << "alpha      : "
		 << alphaFirstRule << ", "
		 << alphaSecondRule << ", "
		 << alphaThirdRule << ", "
		 << alphaFourthRule << ", "
		 << alphaFifthRule << ", "
		 << alphaSixthRule << ", "
		 << alphaSeventhRule << ", "
		 << alphaEighthRule << ", "
		 << alphaNinthRule << " = "
		 << (double)(alphaFirstRule + alphaSecondRule + alphaThirdRule + alphaFourthRule + alphaFifthRule + alphaSixthRule + alphaSeventhRule + alphaEighthRule + alphaNinthRule)
		 << endl;
	cout << "z          : "
		 << highHelloIntervalTime << ", "
		 << highHelloIntervalTime << ", "
		 << mediumHelloIntervalTime << ", "
		 << highHelloIntervalTime << ", "
		 << mediumHelloIntervalTime << ", "
		 << lowHelloIntervalTime << ", "
		 << mediumHelloIntervalTime << ", "
		 << lowHelloIntervalTime << ", "
		 << lowHelloIntervalTime << " = "
		 << (double)(alphaFirstRule + alphaSecondRule + alphaThirdRule + alphaFourthRule + alphaFifthRule + alphaSixthRule + alphaSeventhRule + alphaEighthRule + alphaNinthRule)
		 << endl;
	cout << "alpha * z  : "
		 << resultFirstRule << ", "
		 << resultSecondRule << ", "
		 << resultThirdRule << ", "
		 << resultFourthRule << ", "
		 << resultFifthRule << ", "
		 << resultSixthRule << ", "
		 << resultSeventhRule << ", "
		 << resultEighthRule << ", "
		 << resultNinthRule << " = "
		 << (double)(resultFirstRule + resultSecondRule + resultThirdRule + resultFourthRule + resultFifthRule + resultSixthRule + resultSeventhRule + resultEighthRule + resultNinthRule)
		 << endl;

	return (double)(resultFirstRule + resultSecondRule + resultThirdRule + resultFourthRule + resultFifthRule + resultSixthRule + resultSeventhRule + resultEighthRule + resultNinthRule) /
		   (double)(alphaFirstRule + alphaSecondRule + alphaThirdRule + alphaFourthRule + alphaFifthRule + alphaSixthRule + alphaSeventhRule + alphaEighthRule + alphaNinthRule);
}

int main()
{
	double NodeSize;
	double Mobility;

	cout << "Masukkan NodeSize (50, 100, 150): ";
	cin >> NodeSize;
	cout << "Masukkan Mobility (0, 2.5, 5): ";
	cin >> Mobility;

	double result = deffuzifikasi(NodeSize, Mobility);
	cout << "Result     : " << result << endl;
	return 0;
}